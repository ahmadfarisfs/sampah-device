from daur_rvm_sm import fsm
from daur_rvm_sm.enum import DaurState,DaurTrigger
from daur_core.core import DaurDevice


    

daur_core = DaurDevice("COM1",9600)
daur_fsm = fsm.TypeSafeFSM(DaurState,DaurState.IDLE)

if __name__ == "__main__":

    daur_fsm.add_transition(DaurTrigger.QR_SCANNED,DaurState.IDLE,DaurState.QR_CHECK)
    daur_fsm.add_transition(DaurTrigger.QR_CHECK_INVALID,DaurState.QR_CHECK,DaurState.INVALID_USER)
    daur_fsm.add_transition(DaurTrigger.QR_CHECK_ADMIN,DaurState.QR_CHECK,DaurState.ADMIN_MODE)
    daur_fsm.add_transition(DaurTrigger.QR_CHECK_VALID,DaurState.QR_CHECK,DaurState.TRASH_INPUT)
    daur_fsm.add_transition(DaurTrigger.USER_INPUT,DaurState.INVALID_USER,DaurState.IDLE)

    daur_fsm.register_state_callback(DaurState.IDLE,None,None)
    daur_fsm.register_state_callback(DaurState.QR_CHECK,None,None)
    