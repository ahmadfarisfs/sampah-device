from threading import Thread
from threading import Event, Lock
import time
import serial
from serial_parser import SerialCommandParser
import logging
from enum import Enum
class ErrorCode(Enum):
    OK = 0
    INVALID_TYPE = 1
    INVALID_ID = 2
    NO_RESPOND = 3
    DRIVER_FAILURE = 4
    HW_ERROR = 5


class ProximitySensorType(Enum):
    INFRARED = 0
    INDUCTIVE = 1
    CAPACITIVE = 2
    PET = 3


class DistanceSensorType(Enum):
    INFRARED = 0
    ULTRASONIC = 1


class SensorLocation(Enum):
    TOP = 0
    MIDDLE = 1
    BOTTOM = 2
    BIN = 4


class MechanismState(Enum):
    OPEN = 1
    CLOSE = 0
    ERROR = 2
    ERR_OPEN = 3
    ERR_CLOSE = 4
    START_MOVE = 5
    STOP_MOVE = 6


class MechanismType(Enum):
    TOP_SHUTTER = 0
    BOTTOM_SHUTTER = 1
    REJECTOR = 2
    SKU_SCANNER = 3
    BIN_LOCK = 4
    BOTTLE_ROLLER = 5


class DaurDevice(object):
    """
    A class for communicating with DaurCore Hardware
    """
    class CmdString(Enum):
        QR_EVENT = "Q:"
        RETURN_EVENT = "T:"
        READ_PROXIMITY = "P:"
        READ_DISTANCE = "D:"
        SET_LIGHTING = "L:"
        SET_MECH_STATE = "S:"
        GET_MECH_STATE = "G:"

        PING = "I:"

    def __init__(self, port_name, baud_rate):
        """
            Constructor
        """
        self.port = port_name
        self.baud_rate = baud_rate
        self.timeout_ms = 500
        self.running = True
        self.serial_dev = serial.Serial()
        self.serial_dev.port = self.port
        self.serial_dev.baudrate = self.baud_rate

        self.barcode_cb = None
        self.ser_watch_thread = Thread(
            target=self._ser_watch_loop, daemon=True)
        self.serial_parser = SerialCommandParser()
        self.serial_return_event = Event()
        self.serial_return_event.clear()
        self.serial_parser.add_serial_event(
            DaurDevice.CmdString.QR_EVENT.value, self._barcode_event_cb)
        self.serial_parser.add_serial_event(
            DaurDevice.CmdString.RETURN_EVENT.value, self._ret_cb)
        self.serial_return_payload = None
        self.lock = Lock()
        self.physical_sensor_mapping = {
            SensorLocation.TOP: {
                ProximitySensorType.INDUCTIVE: 0,
                ProximitySensorType.PET: 1,  # reserved
                ProximitySensorType.CAPACITIVE: 2,
                ProximitySensorType.INFRARED: 3,
                DistanceSensorType.ULTRASONIC: 4,
                DistanceSensorType.INFRARED: 5
            },
            SensorLocation.MIDDLE: {
                ProximitySensorType.INDUCTIVE: 6,
                ProximitySensorType.PET: 7,  # reserved
                ProximitySensorType.CAPACITIVE: 8,
                ProximitySensorType.INFRARED: 9,
                DistanceSensorType.ULTRASONIC: 10,
                DistanceSensorType.INFRARED: 11
            },
            SensorLocation.BOTTOM: {
                ProximitySensorType.INDUCTIVE: 12,
                ProximitySensorType.PET: 13,  # reserved
                ProximitySensorType.CAPACITIVE: 14,
                ProximitySensorType.INFRARED: 15,
                DistanceSensorType.ULTRASONIC: 16,
                DistanceSensorType.INFRARED: 17
            },
            SensorLocation.BIN: {
                DistanceSensorType.ULTRASONIC: 18,
                DistanceSensorType.INFRARED: 19
            }
        }

    def _barcode_event_cb(self, payload):
        if self.barcode_cb is None:
            logging.debug("Barcode Detected but Callback not set")
        else:
            self.barcode_cb(payload)
            logging.debug("Barcode Detected")

    def _ret_cb(self, payload):
        self.serial_return_payload = payload
        self.serial_return_event.set()
        logging.debug("Serial Return Event")

    def _ser_watch_loop(self):
        while(self.running):
            bytes_read = self.serial_dev.readline()
            self.serial_parser.append_string(bytes_read.decode())

    def open_device(self):
        if not self.serial_dev.is_open:
            print("port not open")
            try:
                self.serial_dev.open()
            except Exception:
                logging.error("Fail to open port", exc_info=True)
                return ErrorCode.DRIVER_FAILURE
            else:
                logging.debug("Device open success, waiting for Core to boot")
                self.ser_watch_thread.start()
                time.sleep(5)
                status = self._check_device_validity()
                if status:
                    logging.debug("Device OK")
                    return ErrorCode.OK
                else:
                    logging.debug(
                        "Device does not respond, but port can be opened")
                    return ErrorCode.NO_RESPOND
        else:
            logging.debug("Device already open")
            return ErrorCode.OK

    def close_device(self):
        if self.serial_dev.is_open:
            try:
                self.serial_dev.close()
            except Exception:
                logging.error("Fail to close device", exc_info=True)
                return ErrorCode.DRIVER_FAILURE
            else:
                logging.debug("Device close success")
                self.running = False
                return ErrorCode.OK
        else:
            logging.debug("Device already closed")
            return ErrorCode.OK
        return

    def _map_sensor(self, sensor_type, sensor_location):
        sensor_id = None
        try:
            sensor_id = self.physical_sensor_mapping[sensor_location][sensor_type]
        except KeyError:
            return None
        return sensor_id

    def read_distance_sensor(self, sensor_type, sensor_location):
        sensor_id = self._map_sensor(sensor_type, sensor_location)
        if sensor_id is None:
            return None, ErrorCode.INVALID_TYPE

        self.lock.acquire()
        self._serial_write(
            DaurDevice.CmdString.READ_DISTANCE.value+str(sensor_id))
        ret = self._wait_serial_reply()
        value = self.serial_parser.parse_float(self.serial_return_payload)
        self.lock.release()

        if ret:
            return value, ErrorCode.OK
        else:
            return None, ErrorCode.NO_RESPOND

    def read_proximity_sensor(self, sensor_type, sensor_location):
        sensor_id = self._map_sensor(sensor_type, sensor_location)
        if sensor_id is None:
            return None, ErrorCode.INVALID_TYPE

        self.lock.acquire()
        self._serial_write(
            DaurDevice.CmdString.READ_PROXIMITY.value+str(sensor_id))
        ret = self._wait_serial_reply()
        value = self.serial_parser.parse_bool(self.serial_return_payload)
        self.lock.release()

        if ret:
            return value, ErrorCode.OK
        else:
            return None, ErrorCode.NO_RESPOND

    def set_mechanism_state(self, mechanism_id, state):
        self.lock.acquire()
        self._serial_write(
            DaurDevice.CmdString.SET_MECH_STATE.value+str(mechanism_id)+str(state))
        ret = self._wait_serial_reply()
        parsed = self.serial_parser.parse_bool(self.serial_return_payload)
        self.lock.release()
        if ret:
            if not parsed:
                return ErrorCode.HW_ERROR
            return ErrorCode.OK
        else:
            return ErrorCode.NO_RESPOND

    def get_mechanism_state(self, mechanism_id):
        self.lock.acquire()
        self._serial_write(
            DaurDevice.CmdString.GET_MECH_STATE.value+str(mechanism_id))
        ret = self._wait_serial_reply()
        parsed = self.serial_parser.parse_int(self.serial_return_payload)
        self.lock.release()
        if ret:
            # see MechanismState enum
            return parsed
        else:
            return None

    def set_comm_timeout(self, timeout_ms):
        self.timeout_ms = timeout_ms
        return

    def get_comm_timeout_s(self):
        return self.timeout_ms

    # def register_barcode_callback(self,cb):
      #  pass

    def register_barcode_callback(self, cb):
        '''
        qr callback must be a function with one input parameter (string)
        '''
        self.barcode_cb = cb
        return

    def _serial_write(self, data_string):
        data_send = data_string+str("\r\n")
        self.serial_dev.writelines([data_send.encode()])

    def _wait_serial_reply(self):
        if self.serial_return_event.wait(self.timeout_ms/1000):
            self.serial_return_event.clear()
            return True
        else:
            return False

    def _check_device_validity(self):
        self.lock.acquire()
        self._serial_write(DaurDevice.CmdString.PING.value)
        ret = self._wait_serial_reply()
        parsed = self.serial_parser.parse_bool(self.serial_return_payload)
        self.lock.release()
        if ret:
            return parsed
        else:
            return False
