from serial_parser import SerialCommandParser
import unittest
from unittest.mock import Mock

class TestParser(unittest.TestCase):
    def my_cb(self):
        self.cbed = True

    def setUp(self):
        self.cbed = False
        self.parser = SerialCommandParser()
        
    def test_parsing(self):
        f = Mock()
        g = Mock()
        self.parser.add_serial_event("R",f)
        self.parser.add_serial_event("Q",g)
        
        self.parser.append_string("R")
        self.parser.append_string(":")
        self.parser.append_string("1")
        self.parser.append_string("\r")
        self.parser.append_string("\n")
        
        self.parser.append_string("Q")
        self.parser.append_string(":")
        self.parser.append_string("100")
        self.parser.append_string("\r")
        self.parser.append_string("\n")
        g.assert_called()
        f.assert_called()
        