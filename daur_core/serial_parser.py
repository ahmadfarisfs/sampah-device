import logging
from multiprocessing import pool


class SerialCommandParser(object):
    def __init__(self, token="\n", prefix_len=2, buffer_len=30):
        self.token = token
        self.buffer = ""
        self.buffer_len = buffer_len
        self.cmd_prefix_len = prefix_len
        self.cmd_dict = {}

    def add_serial_event(self, prefix_string, callback):
        '''
        Callback should be a function with one input parameter
        '''
        self.cmd_dict.update({prefix_string: callback})
        return

    def parse_float(self, payload):
        return float(payload)

    def parse_int(self, payload):
        return int(payload)

    def parse_bool(self, payload):
        if payload == "1":
            return True
        elif payload == "0":
            return False
        else:
            return None

    def append_string(self, terminated_string):
        if terminated_string[len(terminated_string)-1] == self.token:
            prefix = terminated_string[:self.cmd_prefix_len]
            if prefix in self.cmd_dict:
                # found valid callback
                callback_f = self.cmd_dict[prefix]
                callback_f(terminated_string[self.cmd_prefix_len:].strip())
                #self.buffer = ""
                return True
            else:
                # no registered cb
                #self.buffer = ""
                return False
        else:
            return False


'''    def append_string(self,new_string):
        if new_string[len(new_string)-1] == self.token:
            completed_string = self.buffer
            prefix = completed_string[:self.cmd_prefix_len]
            if prefix in self.cmd_dict:
                #found valid callback
                callback_f = self.cmd_dict[prefix]
                callback_f(completed_string[self.cmd_prefix_len:])
                self.buffer = ""
                return True
            else:
                #no registered cb
                self.buffer = ""
                return None
                        
        else:
            if self.buffer_len > len(self.buffer):
                # append string to buffer
                self.buffer += new_string
            else:
                # warning: token not found until buffer depleted, reset buffer
                self.buffer = ""
            return None'''
