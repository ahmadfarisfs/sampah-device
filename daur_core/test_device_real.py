from daur_device import DaurDevice, ErrorCode, SensorLocation, ProximitySensorType, DistanceSensorType
import serial
from threading import Thread
import time
'''opened=False
serial_dev = serial.Serial()
def ulala():
    while(opened):
        print("wait")
        a = serial_dev.readline()
        print(a)
        time.sleep(1)



getter = Thread(target=ulala)
serial_dev.port = "COM14"
serial_dev.baudrate =57600
serial_dev.open()
opened=True
getter.start()

while(True):
    continue
'''

device = DaurDevice("COM14",57600)
device.set_comm_timeout(500)
err_code = device.open_device()
print(err_code)
distance,err_code = device.read_distance_sensor(DistanceSensorType.INFRARED,SensorLocation.TOP)
print(distance,err_code)
distance,err_code = device.read_distance_sensor(DistanceSensorType.INFRARED,SensorLocation.BOTTOM)
print(distance,err_code)
exist,err_code = device.read_proximity_sensor(ProximitySensorType.INDUCTIVE, SensorLocation.MIDDLE)
print(exist,err_code)
