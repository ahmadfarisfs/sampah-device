from daur_device import DaurDevice, ErrorCode
import unittest


class TestDevice(unittest.TestCase):
    def setUp(self):
        self.device = DaurDevice("COM14",57600)

    def test_open(self):
        err_code = self.device.open_device()
        self.device.set_comm_timeout(100000)
        self.assertEqual(err_code, ErrorCode.OK)
