class ScanningChamber(object):
    def __init__(self,hw_controller):
        self.hw_contr = hw_controller
        
    def open_chamber(self):
        pass
    
    def close_chamber(self):
        pass

    def reject_payload(self):
        pass
    
    def accept_payload(self):
        pass

    def is_empty(self):
        pass
    
    def register_trash_insert_cb(self,callback):
        pass
    