from enum import Enum
from daur_core.core import ProximitySensorType, SensorLocation, RangeSensorType, MechanismState, MechanismType

class AiValidator(object):
    def __init__(self, hw_controller, image_validator, sku_validator):
        self.hw_cont = hw_controller
        self.image_validator = image_validator
        self.sensor_level = 2
        self.sensor_loc_arr = [SensorLocation.BOTTOM, SensorLocation.MIDDLE,SensorLocation.TOP]
        self.range_threshold = None
        self.sku_validator = sku_validator
        self.buffered_sku = []
        self.ready= False

    def start(self):
        self.sku_validator.start()
        #self.hw_cont.set_mechanism_state(MechanismType.BOTTLE_ROLLER,MechanismState.START_MOVE)
        self.ready = True
    
    def stop(self):
        self.sku_validator.stop()
        #self.hw_cont.set_mechanism_state(MechanismType.BOTTLE_ROLLER,MechanismState.STOP_MOVE)
        self.ready = False

    def set_sensor_check_level(self, level):
        """
        Level 0 : bottom only
        Level 1 : bottom + middle 
        Level 2 : bottom + middle + top 
        """
        # TODO: check level bound
        self.sensor_level = level
    
    def set_dimensional_threshold(self, distance):
        self.range_threshold = distance
    
    def is_sku_valid(self):
        return self.sku_validator.validate()

    def is_dimensionally_correct(self):
        ranges = []
        for i in self.sensor_loc_arr[:self.sensor_level]:
            dist = self.hw_cont.read_range_sensor(RangeSensorType.ULTRASONIC,i)
            if dist < self.range_threshold:
                ranges.append(True)
            else:
                ranges.append(False)

        for i in self.sensor_loc_arr[:self.sensor_level]:
            dist = self.hw_cont.read_range_sensor(RangeSensorType.INFRARED,i)
            if dist < self.range_threshold:
                ranges.append(True)
            else:
                ranges.append(False)
        
        return all(ranges)
    
    def validate(self):
        # TODO: make this async
        result = False
        if self.ready:
            image_result = self.image_validator.validate()
            if image_result and (not self.is_metal()) and self.is_plastic() and self.is_dimensionally_correct() and self.is_sku_valid():
                result = True
        else:
            # not started yet
            return False
        return result

    def is_metal(self):
        metal = []
        for i in self.sensor_loc_arr[:self.sensor_level]:
            metal.append(self.hw_cont.read_presence_sensor(ProximitySensorType.INDUCTIVE,i))
        return all(metal)

    def is_plastic(self):
        plastic = []
        for i in self.sensor_loc_arr[:self.sensor_level]:
            plastic.append(self.hw_cont.read_presence_sensor(ProximitySensorType.CAPACITIVE,i))
        return all(plastic)
    

    
