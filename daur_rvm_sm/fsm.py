from transitions import Machine
import types
import logging

class TypeSafeFSM(object):
    def __init__(self,states_enum, initial_state):
        self.states = []
        self.state_num = states_enum
        for i in range(len(self.state_num)):
            a= self.state_num(i).name
            self.states.append(a)
        self.machine = Machine(model=self,initial=initial_state.name,states=self.states,ignore_invalid_triggers=True)
        self.callback_dict = {}
        self.previous_state=initial_state.name
    
    def add_transition (self, trigger, source, dest):
        self.machine.add_transition(trigger=trigger.name, source=source.name, dest=dest.name)
        
    def register_state_callback(self, state, on_enter, on_exit):
        self.callback_dict[state.name]  =  {"on_enter": on_enter, "on_exit": on_exit}
            
    def accept_trigger(self,trigger_enum):
        trigger = getattr(self,trigger_enum.name)
        trigger()
        if self.previous_state != self.get_state_name():
            logging.debug("Trigger accepted, change state from %s to %s" %(self.previous_state,self.get_state_name()))
            # state changed
            if self.previous_state in self.callback_dict.keys():
                on_exit_cb = self.callback_dict[self.previous_state]['on_exit'] 
                if on_exit_cb is not None:
                    logging.debug("On exit callback %s : start " % self.previous_state)
                    on_exit_cb()
                    logging.debug("On exit callback %s : end " % self.previous_state)
        
            if self.get_state_name() in self.callback_dict.keys():
                on_enter_cb = self.callback_dict[self.get_state_name()]['on_enter'] 
                if on_enter_cb is not None:
                    logging.debug("On enter callback %s : start " %self.get_state_name())
                    on_enter_cb()
                    logging.debug("On enter callback %s : end " %self.get_state_name())

            self.previous_state = self.get_state_name()
        else:
            # state not changed            
            logging.debug("Trigger accepted but state not changed")

    def get_state_name(self):
        return getattr(self,'state')

    def get_state_enum(self):
        return self.state_num(self.states.index(self.get_state_name()))
"""
objs = TypeSafeFSM(DaurState,DaurState.IDLE)

objs.add_transition(DaurTrigger.QR_SCANNED,DaurState.IDLE,DaurState.QR_CHECK)
objs.add_transition(DaurTrigger.TRASH_INPUT_TIMEOUT,DaurState.QR_CHECK,DaurState.IDLE)

def qr_enter_cb():
    print("qr check enter")

def qr_exit_cb():
    print("qr check exit")

def exit_idle():
    print("idle  exit")
        

objs.add_state_callback(DaurState.QR_CHECK,qr_enter_cb,None)
objs.add_state_callback(DaurState.IDLE,None,exit_idle)

print(objs.get_state())
objs.accept_trigger(DaurTrigger.QR_SCANNED)
print(objs.get_state())
objs.accept_trigger(DaurTrigger.TRASH_INPUT_TIMEOUT)
print(objs.get_state())
"""