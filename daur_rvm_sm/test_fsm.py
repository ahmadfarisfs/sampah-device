from fsm import TypeSafeFSM
import unittest
from enum import Enum

class StateTest(Enum):
    ZERO=0
    ONE=1
    TWO=2

class TriggerTest(Enum):
    ZERO_TO_ONE=0
    ONE_TO_TWO=1

class TestFSM(unittest.TestCase):
    def setUp(self):
        self.fsm=TypeSafeFSM(StateTest,StateTest.ZERO) 
        self.fsm.add_transition(TriggerTest.ZERO_TO_ONE,StateTest.ZERO,StateTest.ONE)
        self.fsm.add_transition(TriggerTest.ONE_TO_TWO,StateTest.ONE,StateTest.TWO)
        self.enter = False
        self.exit = False 
        
    def test_init(self):
        self.assertEqual(self.fsm.get_state_enum(),StateTest.ZERO)
    
    def test_transition(self):
        self.fsm.accept_trigger(TriggerTest.ZERO_TO_ONE)
        self.assertEqual(self.fsm.get_state_enum(),StateTest.ONE)
        self.fsm.accept_trigger(TriggerTest.ONE_TO_TWO)
        self.assertEqual(self.fsm.get_state_enum(),StateTest.TWO)
        self.fsm.accept_trigger(TriggerTest.ZERO_TO_ONE)
        self.assertEqual(self.fsm.get_state_enum(),StateTest.TWO)
        

        
if __name__ == '__main__':
    unittest.main()